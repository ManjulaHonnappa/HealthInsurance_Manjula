
enum CurrentHealth
{
	HYPERTENSION, BLOODPRESSURE, BLOODSUGAR, OVERWEIGHT;
}

enum Habits
{
	SMOKING, ALCOHOL, DAILYEXERCISE, DRUGS;
}

public class HealthInsuranceQuotation {
	
	private String name; 
	private String  gender;
	private int age;
	// Base premium for anyone below the age of 18 years = Rs. 5,000
	private double premiumAmount = 5000;
	private int genderPercentage = 0;
	private int agePercentage = 0;
	private int habitPercentage = 0;
	private int currentHealthPercentage = 0;
	
	
	public HealthInsuranceQuotation () {
		
	}
	
	public HealthInsuranceQuotation (String name, String gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}
	
	public double premiumCalculation (CurrentHealth currentHealth, Habits habits) {
		calculatePremiumBaseOfAge ();
		calculatePremiumBaseOnGender();
		calculatePremiumBaseOnPreExisting (currentHealth);
		calculatePremiumBaseOnHabits (habits);
		return premiumAmount;
	}
	
	
	/**
	 * Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
	 */
	private void calculatePremiumBaseOnGender () {
		// for gender 
		if ("M".equals(gender)) {
			genderPercentage = 2;
		}
		premiumAmount += premiumAmount*(.02);
	}
	
	/**
	 * % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% 
	 * | 35-40 -> +10% | 40+ -> 20% increase every 5 years
	 */
	private void calculatePremiumBaseOfAge() {
		if (age > 18 && age <=25) {
			premiumAmount += premiumAmount *(.10);
		}
		if (age > 25 || age ==30) {
			premiumAmount += premiumAmount *(.10);
		} 
		if (age > 30 || age ==35) {
			premiumAmount += premiumAmount *(.10);
		}
		if (age >= 35 || age == 40) {
			premiumAmount += premiumAmount *(.10);
		}
		if (age > 40) {
			premiumAmount += premiumAmount *(.20);
		}
			
	}
	
	/**
	 * Pre-existing conditions (Hypertension | Blood pressure | Blood sugar | Overweight)
	 *  -> Increase of 1% per condition
	 * @param habit
	 */
	private void calculatePremiumBaseOnPreExisting (CurrentHealth currentHealth) {
		if (currentHealth == CurrentHealth.HYPERTENSION 
				|| currentHealth == CurrentHealth.BLOODPRESSURE
				|| currentHealth == CurrentHealth.BLOODSUGAR
				|| currentHealth == CurrentHealth.OVERWEIGHT) {
			premiumAmount += premiumAmount *(.10);
		}
	}

	/**
	 * Habits
	 * Good habits (Daily exercise) -> Reduce 3% for every good habit
	 * Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
	 * @param habit
	 */
	private void calculatePremiumBaseOnHabits (Habits habit) {
		double interestAmt = premiumAmount * (0.003);
		if (habit == Habits.DAILYEXERCISE) {
			premiumAmount = premiumAmount - interestAmt;
		} else {
			premiumAmount = premiumAmount + interestAmt;
		}
	}
	
	public static void main (String []str) {
		/** 
		 *   Input
		 *   Current health:
	 	 *		Hypertension: No
		 *		Blood pressure: No
		 *		Blood sugar: No
		 *		Overweight: Yes
		 *
		 *	Habits:
		 *		Smoking: No
		 *  	Alcohol: Yes
		 *  	Daily exercise: Yes
		 *  	Drugs: No
		 */
		HealthInsuranceQuotation hIQ = new HealthInsuranceQuotation("Norman Gomes", "M", 34);
		System.out.println(hIQ.premiumCalculation(CurrentHealth.valueOf("HYPERTENSION"), Habits.valueOf("DAILYEXERCISE")));
	}
}
