import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HealthInsuranceQuotationTest extends junit.framework.TestCase{

	private HealthInsuranceQuotation healthInsuranceQuotation;
	

	/** * Initialization */
	@Before
	public void setUp() {
		healthInsuranceQuotation = new HealthInsuranceQuotation("Norman Gomes", "M", 34);
	}

	/** * Test case for add method */
	@Test
	public void testCalculation() {
		double actual = healthInsuranceQuotation.premiumCalculation(CurrentHealth.BLOODPRESSURE, Habits.DAILYEXERCISE);
		assertEquals(6767,((int)actual));
	}

	/** * destroy the object */
	@After
	public void tearDown() {
	}
}
